# Timecheck

Timecheck is a tool for controlling the usage time on a linux computer. 


[Deutsche README](https://codeberg.org/nuron/timecheck/src/branch/master/README_de.md "README_de.md")

---


Timecheck gives you the possibility to control the usage time of a specific user account.
It is a small shell script which should work in every shell as it is written based on the POSIX standard.

A speciality of Timecheck is that the days of a week will be grouped.

Per default a week will be splitted into Monday until Friday and Saturday and Sunday.
These two groups can have their own, independant times.

Per default Timecheck sends a notification to the user ten, five and one minute before the user get logged out.
Of course these notification times can be edited and extended.

See below for more information about possible adjustments.


---

#### Important informations

Since the project is in the beginning the script is quite rudimentary.
Prinicipally the script already works.
The script was tested and developed for Linux Mint Cinnamon.
**Maybe you have to make some adjustments for other distributions and/or desktop environments!**

The script has been adapted for German or English systems. For other systems there might are adjustments necessary.

At the moment the notifications are in English. If you want you can edit the notifications.


---

## Dependencies

For a full usage of the script the following applications have to be installed:

- sudo 
- libnotify-bin (notify-send)

## Usage

In the meantime it exists an install script for Timecheck which will do some steps for you.
For starting you have to clone the repository manually.
The easiest way to do this is the following:

```bash
git clone https://codeberg.org/nuron/timecheck.git timecheck
```

For the sake of convenience we switch into the directory `timecheck` where the script is located.

```bash
cd timecheck/
```

Inside this directory we execute the install script as follows:

```bash
./install-timecheck.sh
```

This file should already have the required permissions.
If not, just run `chmod +x install-timecheck.sh` to add execute permission.

The script asks for several things and will configure all the necessary things at the end.

At the end there will be a note to copy something into the root crontab. This is esentially to use this script!

If you've done everything there's nothing to do anymore.

### Adjustments

Maybe you have to adapt the script.

##### Ath the moment there are no known adjustments

If you have any adjustment please give some feedback.

#### This script is released under the GPLv3 license. 

See COPYING file for more information
