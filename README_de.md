# Timecheck

### Timecheck ist ein Tool, um die Nutzungszeit auf einem Linux Computer zu kontrollieren


--- 


Timecheck bietet dir die Möglichkeit, die Nutzungszeit von einem bestimmen Nutzeraccount zu kontrollieren. 
Es ist ein kleines Shell-Script, welches in jeder Shell funktionieren sollte, da es nach POSIX Standards geschrieben wurde. 

Eine Besonderheit von Timecheck ist es, dass die Tage einer Woche gruppiert werden. 

Standardmäßig wird eine Woche in Montag bis Freitag, und Samstag und Sonntag aufgeteilt.
Diese beiden Gruppen können jeweils eigene, unabhängige Zeiten zugeteilt bekommen. 

Standardmäßig verschickt Timecheck zehn, fünf und eine Minute vor Abmeldung eine Benachrichtigung an den Nutzer. 
Natürlich können diese Abstände beliebig verändert und erweitert werden. 

Mehr zu eventuellen Anpassungen siehe unten


---

#### Wichtige Information
Da dieses Projekt erst am Anfang steht, ist das Script noch entsprechend rudimentär.
Prinzipiell funktioniert es jedoch schon. 
Getestet und entwickelt wurde das Script für Linux Mint Cinnamon. 
**Eventuell müssen für andere Distributionen und/oder Desktop-Umgebungen kleine Anpassungen getätigt werden!**

Das Script ist für deutsche oder englische Systeme angepasst. Für anders eingestellte Systeme müssen gegebenenfalls Änderunge vorgenommen werden!

Die Benachrichtigungen sind im Moment standardmäßig auf englisch. Bei Bedarf können die Benachrichtigungen selbst angepasst werden.

---

## Abhängigkeiten

Um das Script in vollem Umfang nutzen zu können werden folgende weitere Anwendungen benötigt:

- sudo
- libnotify-bin (notify-send)

## Nutzung

Für Timecheck existiert mittlerweile ein install-Script, welches dir einige Schritte ab nimmt. 
Jedoch musst du das repository zu Beginn händisch clonen. 
Am Einfachsten geht das mit

	git clone https://codeberg.org/nuron/timecheck.git timecheck

Der Einfachheit halber wechseln wir nun in das Verzeichnis `timecheck` in dem das Script nun liegt

	cd timecheck/

Hier führen wir nun die `install-timecheck.sh` Datei aus. 

	./install-timecheck.sh

Diese sollte schon die benötigten Berechtigungen besitzen. 
Wenn nicht hilft `chomod +x install-timecheck.sh` hier weiter. 

Das Script fragt nun eine Reihe von Sachen ab und configuriert im Anschluss alles nötige.

Am Ende weißt es noch einmal darauf hin, etwas in die root crontab zu kopieren. Das ist essenziell für die Nutzung!

Wenn das geschehen ist, steht der Nutzung nichts mehr im Wege!


### Anpassungen

Das Script muss eventuell an dein System angepasst werden.

##### Aktuell sind keine nötigen Anpassungen bekannt. 
Solltest du auf welche stoßen gib bitte bescheid!

--- 

#### Dieses Script ist unter der GPLv3 Lizenz freigegeben
Beachte die COPYING Datei für weitere Informationen
